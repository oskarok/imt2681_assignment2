package repocheck

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"
)

var StartTime time.Time

//Gets the status code from api
func getStatus(w http.ResponseWriter, url string) int {

	result, err := http.Get(url)
	if err != nil {
		http.Error(w, "Could not retrieve from server", http.StatusBadRequest)
	}
	return result.StatusCode
}

// Sets the status info
func HandlerStatus(w http.ResponseWriter, r *http.Request) {

	//Sets version to second "part" of url"
	parts := strings.Split(r.URL.Path, "/")
	version := parts[2]

	urlGit := "https://git.gvk.idi.ntnu.no/api/"
	//urlDb := "???????"

	var stat Status

	stat.Gitlab = getStatus(w, urlGit)
	//stat.DB = getStatus(w, urlDb)

	stat.DB = 200 //Assumes it to be ok
	//Doc("0") is reserved for status checks
	_, err := Db.Client.Collection("webhooks").Doc("0").Get(Db.Ctx)
	if err != nil {
		//Can not get to server for unknown reason
		//Gives a Service Unavailable error
		stat.DB = 503
	}

	stat.Version = version

	stat.Uptime = float64(time.Since(StartTime).Seconds())

	w.Header().Add("content-type", "application/json")
	err = json.NewEncoder(w).Encode(stat)
	if err != nil {
		http.Error(w, "Could not encode", http.StatusBadRequest)
	}

}
