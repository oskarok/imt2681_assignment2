package repocheck

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"time"
)

func langCounter(l []string) map[string]int {

	count := make(map[string]int)
	for _, i := range l {
		_, exist := count[i]
		if exist {
			//If language already exits add one
			count[i]++
		} else {
			count[i] = 1 // if language does not exist, start from one
		}
	}
	return count
}

func HandlerLanguages(w http.ResponseWriter, r *http.Request) {

	projectsURL := APIurl + "projects?per_page=100"

	var language []string
	var languages []LanguageInfo

	limit := 5
	auth := false

	params := []string{strconv.Itoa(limit)}
	webHooksInvoke("commits", params, time.Now())

	var repos []RepoInfo

	//Sets the limit to what to user input if its not empty
	userLimit := r.FormValue("limit")
	if userLimit != "" {
		if userLim, err := strconv.Atoi(userLimit); err == nil {
			limit = userLim
		} else {
			http.Error(w, "Limit must be an int", http.StatusBadRequest)
			return
		}
	}

	//gets the private token and adds to url
	authToken := r.FormValue("auth")
	if authToken != "" {

		projectsURL = projectsURL + "&private_token=" + authToken

		auth = true
	}

	check := true

	//Gets data from the projects
	for i := 1; check == true; i++ {
		var repo []RepoInfo
		url := projectsURL + fmt.Sprintf("&page=%d", i) //%d replaced by i

		resp, err := http.Get(url)
		if err != nil {
			http.Error(w, "Could not get data", http.StatusBadRequest)
		}

		err = json.NewDecoder(resp.Body).Decode(&repo)
		if err != nil {
			http.Error(w, "Could not decode", http.StatusBadRequest)
		}

		//appends the results to repos variable
		for r := range repo {
			repos = append(repos, repo[r])
		}
		//If there are no more pages loop ends
		if totPages, err := strconv.Atoi(resp.Header.Get("X-Total-Pages")); err == nil {
			if i == totPages {
				check = false
			}
		}
	}

	for i := range repos {

		langURL := APIurl + fmt.Sprintf("projects/%d/languages", repos[i].ID)

		fmt.Println(repos[i].ID)

		//Adds private token if necessary
		if auth == true {
			langURL = langURL + "?private_token=" + authToken
		}

		fmt.Println(langURL)

		resp, err := http.Get(langURL)
		if err != nil {
			http.Error(w, "Could not get languages", http.StatusBadRequest)
		}

		var lang = make(map[string]float32)

		err = json.NewDecoder(resp.Body).Decode(&lang)
		if err != nil {
			http.Error(w, "Could not decode ", http.StatusBadRequest)
		}

		for k := range lang {
			language = append(language, k)
		}
	}

	//Gets total count of languages
	lang := langCounter(language)

	//Adds the count to languages array
	for i, j := range lang {
		var l LanguageInfo
		l.Name = i
		l.Count = j

		languages = append(languages, l)
	}

	sort.SliceStable(languages, func(i, j int) bool { return languages[i].Count > languages[j].Count })

	var result Language

	for i := 0; i < limit; i++ {
		result.LangList = append(result.LangList, languages[i].Name)
	}

	result.Authentication = auth

	w.Header().Add("content-type", "application/json")
	err := json.NewEncoder(w).Encode(result)
	if err != nil {
		http.Error(w, "Could not encode", http.StatusBadRequest)
	}
}
