IMT2681 Assignment2 


Project does not run if you don't have a firebase access key to put in the Init() function in firebase.go. Idk if we were meant to have it uploaded or not but it doesn't seem very secure, so i chose not to. 

The server is accessed through "http://10.212.139.166:8080/repocheck/v1/" and then an endpoint


Endpoints: 


Commits:


use ../repocheck/v1/commits followed by:

?limit={number},

?auth={privateKey}

or ?limit={number}&auth={privateKey}

or leave it empty for default values limit= 5 and no private key.



Languages: 

use ../repocheck/v1/languages followed by:

?limit={number},

?auth={privateKey}

or ?limit={number}&auth={privateKey}

or leave it empty for default values limit= 5 and no private key.


Status: 

use ../repocheck/v1/status

Database status gives wrong value

Webhooks: 

use ../repocheck/v1/webhooks/

POST: 

Register a new webhook by sending a POST request with the body containing the following:

{
    "event" : "commits/languages/status" ,
    "url": "webhook url"
}

GET:

Sending a get request gets you all the webhooks. You can choose to put ../webhooks/(id) to get that specific webhook.

DELETE:

use ../webhooks/(id) with a delete request to delete that webhook.

Webhooks works except for timestamp

