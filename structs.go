package repocheck

import (
	"context"
	"time"

	"cloud.google.com/go/firestore"
)

const APIurl = "https://git.gvk.idi.ntnu.no/api/v4/"

type Result struct {
	Name    string `json:"path_with_namespace"`
	Commits int    `json:"commits"`
}

type RepoInfo struct {
	ID      int    `json:"id"`
	Name    string `json:"path_with_namespace"`
	Commits int    `json:"commits"`
}

type Commits struct {
	Repos          []Result `json:"repos"`
	Authentication bool     `json:"Authentication"`
}

type CommitID struct {
	ID string `json:"id"`
}

type LanguageInfo struct {
	Name  string `json:"language"`
	Count int    `json:"count"`
}

type Language struct {
	LangList       []string `json:"languages"`
	Authentication bool     `json:"auth"`
}

type Status struct {
	Gitlab  int     `json:"gitlab"`
	DB      int     `json:"database"`
	Version string  `json:"version"`
	Uptime  float64 `json:"uptime"`
}

type Webhook struct {
	ID    string `json:"id"`
	Event string `json:"event"`
	URL   string `json:"url"`
	Time  string `json:"time"`
}

type Invocation struct {
	Event  string    `json:"event"`
	Params []string  `json:"params"`
	Time   time.Time `json:"time"`
}

type FirestoreDatabase struct {
	ProjectID      string
	CollectionName string
	Ctx            context.Context
	Client         *firestore.Client
}

type Database interface {
	Init() error
	Close()
	Save(*Webhook) error
	Delete(*Webhook) error
	ReadByID(string) (Webhook, error)
}
