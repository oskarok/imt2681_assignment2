package main

import (
	"fmt"
	"html"
	"log"
	"net/http"
	"os"
	"repocheck"
	"time"
)

func timeInit() {
	repocheck.StartTime = time.Now()
}

func HandlerDefault(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "This is the main page, try: /repocheck/v1/languages"+
		"/repocheck/v1/commits\n"+
		"/repocheck/v1/status\n or"+
		"/repocheck/v1/webhooks", html.EscapeString(r.URL.Path))
}

func main() {

	timeInit()
	const projectID = "assignment2-defb3"
	const collection = "webhooks"

	//Initializes database
	repocheck.Db = repocheck.FirestoreDatabase{ProjectID: projectID, CollectionName: collection}
	err := repocheck.Db.Init()
	if err != nil {
		log.Fatal(err)
	}

	defer repocheck.Db.Close()

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	http.HandleFunc("/", HandlerDefault)
	http.HandleFunc("/repocheck/v1/languages", repocheck.HandlerLanguages)
	http.HandleFunc("/repocheck/v1/commits", repocheck.HandlerCommits)
	http.HandleFunc("/repocheck/v1/status", repocheck.HandlerStatus)
	http.HandleFunc("/repocheck/v1/webhooks/", repocheck.HandlerWebhooks)
	http.HandleFunc("/repocheck/v1/webhooks", repocheck.HandlerWebhooks) //else it chrashes sometimes for some reason

	fmt.Println("Listening on port " + port)
	log.Fatal(http.ListenAndServe(":"+port, nil))

}
