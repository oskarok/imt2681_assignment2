package repocheck

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"time"
)

func HandlerCommits(w http.ResponseWriter, r *http.Request) {

	projectsURL := APIurl + "projects?per_page=100"

	limit := 5
	auth := false
	var repos []RepoInfo

	params := []string{strconv.Itoa(limit)}
	webHooksInvoke("commits", params, time.Now())

	//Sets the limit to what to user input if its not empty
	userLimit := r.FormValue("limit")
	if userLimit != "" {
		if userLim, err := strconv.Atoi(userLimit); err == nil {
			limit = userLim
		} else {
			http.Error(w, "Limit must be an int", http.StatusBadRequest)
			return
		}

	}

	//gets the private token and adds to url
	authToken := r.FormValue("auth")
	if authToken != "" {
		projectsURL = projectsURL + "&private_token=" + authToken
		auth = true
	}

	check := true
	//Gets data from the projects
	for i := 1; check == true; i++ {
		var repo []RepoInfo
		url := projectsURL + fmt.Sprintf("&page=%d", i) //%d replaced by i

		resp, err := http.Get(url)
		if err != nil {
			http.Error(w, "Could not get data", http.StatusBadRequest)
		}

		err = json.NewDecoder(resp.Body).Decode(&repo)
		if err != nil {
			http.Error(w, "Could not decode", http.StatusBadRequest)
		}

		//appends the results to repos variable
		for r := range repo {
			repos = append(repos, repo[r])
		}
		//If there are no more pages loop ends
		if totPages, err := strconv.Atoi(resp.Header.Get("X-Total-Pages")); err == nil {
			if i == totPages {
				check = false
			}
		}
	}

	//Goes through all the repos and gets the commits
	for i := 0; i < len(repos); i++ {

		commitsURL := APIurl + fmt.Sprintf("projects/%d/repository/commits", repos[i].ID)

		fmt.Println(repos[i].ID)

		//Adds private token if necessary
		if auth == true {
			commitsURL = commitsURL + "?private_token=" + authToken
		}

		fmt.Println(commitsURL)

		resp, err := http.Get(commitsURL)
		if err != nil {
			http.Error(w, "Could not get commits", http.StatusBadRequest)
		}

		//Gets total commits in project
		totCommits := resp.Header.Get("X-Total")
		fmt.Println(totCommits)

		//Adds commit count to repo info
		if count, err := strconv.Atoi(totCommits); err == nil {
			repos[i].Commits = count
		}

	}

	//Sorts the results so that the repo with most commits is first
	sort.SliceStable(repos, func(i, j int) bool { return repos[i].Commits > repos[j].Commits })

	var mostCommits Commits

	jsonD, err := json.Marshal(repos[:limit])
	if err != nil {
		http.Error(w, "Error", http.StatusBadRequest)
	}

	err = json.Unmarshal(jsonD, &mostCommits.Repos)
	if err != nil {
		http.Error(w, "Error", http.StatusBadRequest)
	}

	mostCommits.Authentication = auth

	w.Header().Add("content-type", "application/json")
	err = json.NewEncoder(w).Encode(mostCommits)
	if err != nil {
		http.Error(w, "Could not encode", http.StatusBadRequest)
	}

}
